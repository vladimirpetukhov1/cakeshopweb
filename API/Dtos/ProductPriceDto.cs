namespace API.Dtos
{
    public class ProductPriceDto
    {
        public int? Id { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public int MeassureId { get; set; }
        public bool IsMain { get; set; }
    }
}