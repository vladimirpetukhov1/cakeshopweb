﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Dtos
{
    public class PriceToUpdateDto
    {
        public int? Id { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public int MeassureId { get; set; }
        public bool IsMain { get; set; }
    }
}
