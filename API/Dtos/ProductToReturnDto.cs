using Core.Entities;
using System.Collections.Generic;

namespace API.Dtos
{
    public class ProductToReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string PictureUrl { get; set; }
        public int ProductTypeId { get; set; }
        public int ProductBrandId { get; set; }
        public IEnumerable<PhotoToReturnDto> Photos { get; set; }
        public IEnumerable<PriceToReturnDto> Prices { get; set; }
    }
}