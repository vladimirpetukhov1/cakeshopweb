﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Dtos
{
    public class BrandsForRequestDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
