using System.Configuration;
using System.Linq;
using API.Errors;
using API.Helpers;
using Core.Interfaces;
using Infrastructure;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration configuration )
        {
            services
            .AddSingleton<IResponseCacheService, ResponseCacheService>()
            .AddScoped<ITokenService, TokenService>()
            .AddScoped<IPhotoService, PhotoService>()
            .AddScoped<IOrderService, OrderService>()
            .AddTransient<IMailService, MailService>()
            .AddScoped<IPaymentService, PaymentService>()
            .AddScoped<IUnitOfWork, UnitOfWork>()
            .AddScoped<IProductRepository, ProductRepository>()
            .AddScoped<IPriceRepository, PriceRepository>()
            .AddScoped<ITasteRepository,TasteRepository>()
            .AddScoped<IBrandRepository, BrandRepository>()
            .AddScoped<IBrandRepository, BrandRepository>()
            .AddScoped<IProductTypeRepository, ProductTypeRepository>()
            .AddScoped<IBasketRepository, BasketRepository>()
            .AddScoped(typeof(IGenericRepository<>), (typeof(GenericRepository<>)))
            .Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var errors = actionContext.ModelState
                        .Where(e => e.Value.Errors.Count > 0)
                        .SelectMany(x => x.Value.Errors)
                        .Select(x => x.ErrorMessage).ToArray();

                    var errorResponse = new ApiValidationErrorResponse
                    {
                        Errors = errors
                    };

                    return new BadRequestObjectResult(errorResponse);
                };
            })
            .Configure<MailSettings>(configuration.GetSection("MailSettings"));



            return services;
        }
    }
}