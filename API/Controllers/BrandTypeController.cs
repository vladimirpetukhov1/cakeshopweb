﻿using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BrandTypeController : BaseApiController
    {
        private readonly IProductTypeRepository _typeRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BrandTypeController(IProductTypeRepository brandRepository, IUnitOfWork unitOfWork,IMapper mapper)
        {
            _typeRepository = brandRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        // GET: api/<BrandType>
        [HttpGet]
        public async Task<ActionResult<ProductType>> Get([FromQuery] TypeSpecParams specParams)
        {
            var spec = new TypesWithNamesSpecification(specParams);

            var countSpec = new TypesForCountSpecification(specParams);

            var totalItems = await _unitOfWork.Repository<ProductType>().CountAsync(countSpec);

            var types = await _unitOfWork.Repository<ProductType>().ListAsync(spec);

            return Ok(new Pagination<ProductType>(specParams.PageIndex, specParams.PageSize, totalItems, types));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get([FromRoute] int id)
        {
            var brandType = await _typeRepository.GetTypeByIdAsync(id);

            if (brandType == null)
            {
                return BadRequest();
            }

            return Ok(brandType);
        }

        // POST api/<BrandType>
        [HttpPost]
        public async Task<ActionResult<ProductType>> Post([FromBody] ProductTypeDto productBrandType)
        {
            var brandType = _mapper.Map<ProductTypeDto, ProductType>(productBrandType);

            var newBrandType=_unitOfWork.Repository<ProductType>().Add<ProductType>(brandType);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem creating brand type"));

            return newBrandType;
        }

        [HttpPut]
        public async Task<ActionResult<ProductType>> Put([FromBody] ProductType productType)
        {
            var brandType = new ProductType { Name = productType.Name, Id = productType.Id };

            _unitOfWork.Repository<ProductType>().Update(brandType);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating brand type"));

            return productType;
        }

        //TODO: make this method generik
        [HttpGet("names/{name}")]
        public async Task<ActionResult> TypeNameExist(string name)
        {
            var types = await _typeRepository.GetBrandsAsync();

            var exist = types.Select(n => n.Name).Any(n => n == name);

            return Ok(exist);
        }

        // PUT api/<BrandType>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<BrandType>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var brandType = await _unitOfWork.Repository<ProductType>().GetByIdAsync(id);

            _unitOfWork.Repository<ProductType>().Delete(brandType);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting brand type"));

            return Ok();
        }

    }
}
