import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
//42.423512869286235, 25.621206708425056
  mapProps=[
    {
      lat:42.42351,
      lng:25.62120,
      title:'Магазин 3',
      id:3,
      zoom:20
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
