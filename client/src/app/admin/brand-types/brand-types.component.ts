import { TypeParams } from './typeParams';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IType } from 'src/app/shared/models/productType';
import { BrandTypeService } from '../services/brand-type.service';
import { ValidationService } from 'src/app/core/services/validation.service';

@Component({
  selector: 'app-brand-types',
  templateUrl: './brand-types.component.html',
  styleUrls: ['./brand-types.component.scss']
})
export class BrandTypesComponent implements OnInit {

  typeForm: FormGroup;
  totalCount: number;
  pageSize = 0;
  pageNumber = 1;
  types: IType[];
  modalRef?: BsModalRef;
  brandType;
  typeParams = new TypeParams();
  modalTitle: string;
  sortSelected = 'name';
  count = 1;

  constructor(
    private brandService: BrandTypeService,
    private modalService: BsModalService,
    private vs: ValidationService,
    private fb: FormBuilder) {
    this.typeParams = this.brandService.getTypeParams();
  }

  ngOnInit(): void {
    this.loadTypes();

    this.typeForm = this.fb.group({
      'name': new FormControl('', {
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(22)],
        asyncValidators: [this.vs.uniqueTypeName()]
      }),
      'id': new FormControl(null)
    });
  }

  getTypes() {
    this.loadTypes();
  }

  deleteType(id: number) {
    this.brandService.delete(id).subscribe((res) => {
      this.loadTypes();
    })
  }

  loadTypes() {
    this.brandService.getTypes().subscribe(response => {
      console.log(response)
      this.types = response.data;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    })
  }

  onSubmit() {
    if (this.brandType.id) {
      this.brandType = {
        ...this.typeForm.value
      }
      this.brandService.update(this.brandType).subscribe((data) => {
        this.typeForm.reset();
        this.modalRef.hide();
        this.loadTypes();
      })
    } else {

      this.brandType = {
        ...this.typeForm.value
      }
      this.brandService.create(this.brandType).subscribe((data) => {
        this.typeForm.reset();
        this.modalRef.hide();
        this.loadTypes();
      })
    }
  }

  config = {
    keyboard: false,
  };
  //TODO: make modal to separate component
  openModal(brandTypeModal: TemplateRef<any>, title: string, id?: number) {
    this.brandType = { name: null };
    this.modalTitle = title;
    if (id) {
      this.brandService.getById(id).then(res => {
        this.brandType = res
      })
    }
    this.modalRef = this.modalService.show(brandTypeModal, this.config);
  }

  onPageChanged($event: any) {
    const params = this.brandService.getTypeParams();
    if (params.pageIndex !== $event) {
      params.pageIndex = $event;
      this.brandService.setTypeParams(params);
      this.loadTypes();
    }
  }

}
