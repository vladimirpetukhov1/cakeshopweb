import { ActivatedRoute } from '@angular/router';
import { IBrand } from 'src/app/shared/models/brand';
import { Component, OnInit, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { BrandsService } from '../../services/brands.service';
import { ShopParams } from 'src/app/shared/models/shopParams';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { catchError, debounceTime, map, tap } from 'rxjs/operators';
import { from, Observable, of } from 'rxjs';
import { BrandParams } from './brandParams';
import { ValidationService } from 'src/app/core/services/validation.service';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements OnInit {

  categoryForm: FormGroup;
  totalCount: number;
  pageSize = 0;
  pageNumber = 1;
  shopParams: ShopParams;
  brands: IBrand[];
  modalRef?: BsModalRef;
  brand;
  brandParams = new BrandParams();
  sortSelected = 'name';

  modalTitle: string;

  constructor(
    private brandService: BrandsService,
    private modalService: BsModalService,
    private vs: ValidationService,
    private fb: FormBuilder) {
    this.brandParams = this.brandService.getBrandParams();
  }

  ngOnInit(): void {

    this.getBrands();

    this.categoryForm = this.fb.group({
      'name': new FormControl('', {
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(22)],
        asyncValidators: [this.vs.uniqueBrandName()]
      }),
      'id': new FormControl(null)
    });
  }

  onPageChanged($event: any) {
    const params = this.brandService.getBrandParams();
    if (params.pageIndex !== $event) {
      params.pageIndex = $event;
      this.brandService.setBrandParams(params);
      this.getBrands();
    }
  }

  config = {
    keyboard: false,
  };

  //TODO: make modal to separate component
  openModal(template: TemplateRef<any>, title: string, id?: number) {
    this.brand = { name: null };
    this.modalTitle = title;
    if (id) {
      this.brandService.getById(id).then(res => {
        this.brand = res
      })
    }
    this.modalRef = this.modalService.show(template, this.config);
  }

  deleteBrand(id: number) {
    this.brandService.delete(id).subscribe((res) => {
      this.getBrands();
    })
  }

  getBrands() {
    this.brandService.getBrands().subscribe(response => {
      console.log(response)
      this.brands = response.data;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    })
  }

  onSubmit() {
    if (this.brand.id) {
      this.brand = {
        ...this.categoryForm.value
      }
      this.brandService.update(this.brand).subscribe((data) => {
        this.categoryForm.reset();
        this.modalRef.hide();
        this.getBrands();
      })
    } else {

      this.brand = {
        ...this.categoryForm.value
      }
      this.brandService.create(this.brand).subscribe((data) => {
        this.categoryForm.reset();
        this.modalRef.hide();
        this.getBrands();
      })
    }
  }
}


