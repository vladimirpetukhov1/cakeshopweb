import { ProductTastesComponent } from './product-tastes/product-tastes.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { EditProductFormComponent } from './edit-product-form/edit-product-form.component';
import { EditProductPhotosComponent } from './edit-product-photos/edit-product-photos.component';
import { EditPriceComponent } from './price/edit-price/edit-price.component';
import { ProductService } from './services/product.service';
import { MessageService } from 'primeng/api';
import { EditPriceFormComponent } from './price/edit-price-form/edit-price-form.component';
import { AddPriceComponent } from './price/add-price/add-price.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SidebarModule } from 'primeng/sidebar';
import { ListboxModule } from 'primeng/listbox';
import { OrdersModule } from './orders/orders.module';
import { BrandsService } from './services/brands.service';
import { BrandsComponent } from './brands/brands-list/brands.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BrandTypesComponent } from './brand-types/brand-types.component';


const ADMIN_MODULES = [
  OrdersModule,
  CommonModule,
  SharedModule,
  AdminRoutingModule,
  SidebarModule,
  ListboxModule,
  AccordionModule.forRoot(),
]

const ADMIN_SERVICES = [
  ProductService,
  MessageService,
  BrandsService,
  BsModalService
]

const ADMIN_COMPONENTS = [AdminComponent,
  EditProductComponent,
  EditProductFormComponent,
  EditProductPhotosComponent,
  EditPriceComponent,
  EditPriceFormComponent,
  AddPriceComponent,
  ProductListComponent,
  BrandsComponent,
  BrandTypesComponent,
  ProductTastesComponent
];


@NgModule({
  declarations: [ADMIN_COMPONENTS],
  imports: [
    ADMIN_MODULES
  ],
  providers: [ADMIN_SERVICES]
})
export class AdminModule { }
