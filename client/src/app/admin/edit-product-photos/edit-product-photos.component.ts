import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { IProduct, ProductFormValues } from 'src/app/shared/models/product';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ProductService } from '../services/product.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-edit-product-photos',
  templateUrl: './edit-product-photos.component.html',
  styleUrls: ['./edit-product-photos.component.scss']
})
export class EditProductPhotosComponent implements OnInit {
  @Input() product: ProductFormValues;
  progress = 0;
  addPhotoMode = false;

  constructor(
    private productService: ProductService,
    private toast: ToastrService,
    private chRef: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  addPhotoModeToggle() {
    this.addPhotoMode = !this.addPhotoMode;
  }

  uploadFile(file: File) {
    this.productService.uploadImage(file, this.product.id).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          this.progress = Math.round(event.loaded / event.total * 100);
          break;
        // case the upload is done and we got a response back
        case HttpEventType.Response:
          this.product = event.body;
          setTimeout(() => {
            this.progress = 0;
            this.addPhotoMode = false;
          }, 1500);
      }
    }, error => {
      if (error.errors) {
        this.toast.error(error.errors[0]);
      } else {
        this.toast.error('Problem uploading image');
      }
      this.progress = 0;
    });
  }

  deletePhoto(photoId: number) {
    this.productService.deleteProductPhoto(photoId, this.product.id).subscribe(() => {
      const photoIndex = this.product.photos.findIndex(x => x.id === photoId);
      this.product.photos.splice(photoIndex, 1);
    }, error => {
      this.toast.error('Problem deleting photo');
      console.log(error);
    });
  }

  setMainPhoto(photoId: number) {
    this.productService.setMainPhoto(photoId, this.product.id).subscribe((product: ProductFormValues) => {
      this.product = product;
    });
  }

}
