import { IPrice, IPriceToCreate } from '../../shared/models/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ProductFormValues } from '../../shared/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  createProduct(product: ProductFormValues) {
    return this.http.post(this.baseUrl + 'products', product);
  }

  updateProduct(product: ProductFormValues, id: number) {
    return this.http.put(this.baseUrl + 'products/' + id, product);
  }

  addPrice(price: IPriceToCreate, id: number) {
    return this.http.put(this.baseUrl + 'products/' + id + '/price', price);
  }

  removePrice(id:number,priceId:number){
    return this.http.delete(this.baseUrl+ 'products/'+id+'/price/'+priceId);
  }

  updatePrice(price: IPrice) {
    return this.http.put(this.baseUrl + 'products/price/update', price);
  }

  deleteProduct(id: number) {
    return this.http.delete(this.baseUrl + 'products/' + id);
  }

  uploadImage(file: File, id: number) {
    const formData = new FormData();
    formData.append('photo', file, 'image.png');
    return this.http.put(this.baseUrl + 'products/' + id + '/photo', formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  deleteProductPhoto(photoId: number, productId: number) {
    return this.http.delete(this.baseUrl + 'products/' + productId + '/photo/' + photoId);
  }

  setMainPhoto(photoId: number, productId: number) {
    return this.http.post(this.baseUrl + 'products/' + productId + '/photo/' + photoId, {});
  }

  setMainPrice(priceId: number, productId: number) {
    return this.http.post(this.baseUrl + 'products/' + productId + '/price/' + priceId, {});
  }
}
