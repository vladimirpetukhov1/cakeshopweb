import { IMeassure } from './../../shared/models/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MeassureService {


  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<IMeassure[]> { return this.http.get<IMeassure[]>(this.baseUrl + 'products/meassures'); }

  add() { }

  delete() { }

  update() { }

  getById() { }
}
