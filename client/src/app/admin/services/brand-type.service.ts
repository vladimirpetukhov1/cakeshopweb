import { IType } from 'src/app/shared/models/productType';
import { TypeParams } from './../brand-types/typeParams';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IPagination, Pagination } from 'src/app/shared/models/pagination';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BrandTypeService {

  baseUrl = environment.apiUrl;

  typeParams=new TypeParams();

  pagination = new Pagination<IType>();

  constructor(private http:HttpClient) { }

  getTypes() {

    let params = new HttpParams();
    params = params.append('sort', this.typeParams.sort);
    params = params.append('pageIndex', this.typeParams.pageIndex.toString());
    params = params.append('pageSize', this.typeParams.pageSize.toString());

    return this.http.get<IPagination<IType>>(this.baseUrl + 'brandType', { observe: 'response', params })
      .pipe(
        map(response => {
          this.pagination = response.body;
          return this.pagination;
        })
      )
  }

  getById(id: number): Promise<any> {
    let brandType = new Promise<any>((resolve, reject) => {
      this.http.get(this.baseUrl + `brandType/${id}`)
        .toPromise()
        .then(
          res => {
            resolve(res);
          }
        )
        .catch((err) => {
          reject(err)
        })
    });
    return brandType;
  }

  update(brandType) {
    return this.http.put(this.baseUrl+'brandType',brandType);
  }

  create(brandType) {
    return this.http.post(this.baseUrl+'brandType',brandType);
  }

  delete(id:number){
    return this.http.delete(this.baseUrl + 'brandType/' + id);
  }

  isNameUnique(name: string) {
    return this.http.get<boolean>(this.baseUrl + `brandType/names/${name}`);
  }

  setTypeParams(params: TypeParams) {
    this.typeParams = params;
  }

  getTypeParams(){
    return this.typeParams;
  }
}
