import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { BrandsComponent } from './brands/brands-list/brands.component';
import { BrandTypesComponent } from './brand-types/brand-types.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, data: { breadcumb: 'Контролен панел' },
    children: [
      { path: 'products', component: ProductListComponent, data: { breadcrumb: 'Test Errors' } },
      { path: 'product/new', component: EditProductComponent, data: { breadcrumb: 'Нов' } },
      { path: 'product/edit/:id', component: EditProductComponent, data: { breadcrumb: 'Промени' } },
      { path: 'brands', component: BrandsComponent, data: { breadcrumb: 'Категории' } },
      { path: 'types', component: BrandTypesComponent, data: { breadcrumb: 'Видове' } }
    ]
  },

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
