import { OrderCountComponent } from './components/order-count/order-count.component';
import { SelectDropdownComponent } from './components/select-dropdown/select-dropdown.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PagingHeaderComponent } from './components/paging-header/paging-header.component';
import { PagerComponent } from './components/pager/pager.component';
import { OrderTotalsComponent } from './components/order-totals/order-totals.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TextInputComponent } from './components/text-input/text-input.component';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { StepperComponent } from './components/stepper/stepper.component';
import { BasketSummaryComponent } from './components/basket-summary/basket-summary.component';
import { RouterModule } from '@angular/router';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { PhotoWidgetComponent } from './components/photo-widget/photo-widget.component';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { FileUploadModule } from 'primeng/fileupload';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { MeassureNamePipe } from './meassure-name.pipe';
import { ShopMapComponent } from './components/shop-map/shop-map.component';
import { CollapsePanelComponent } from './components/collapse-panel/collapse-panel.component';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { ChipsModule } from 'primeng/chips';
import { TooltipModule } from 'primeng/tooltip';
import { PasswordModule } from 'primeng/password';
import { DividerModule } from 'primeng/divider';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCoffee, fas } from '@fortawesome/free-solid-svg-icons';
import { PanelModule } from 'primeng/panel';


const DECLARATIONS = [
  PagingHeaderComponent,
  PagerComponent,
  OrderTotalsComponent,
  TextInputComponent,
  StepperComponent,
  BasketSummaryComponent,
  PhotoWidgetComponent,
  SelectDropdownComponent,
  MeassureNamePipe,
  ShopMapComponent,
  CollapsePanelComponent,
  OrderCountComponent
];

const IMPORTS = [
  CommonModule,
  PaginationModule.forRoot(),
  CarouselModule,
  BsDropdownModule.forRoot(),
  ReactiveFormsModule,
  FormsModule,
  CdkStepperModule,
  RouterModule,
  NgxGalleryModule,
  TabsModule.forRoot(),
  NgxDropzoneModule,
  ImageCropperModule,
  TableModule,
  ToolbarModule,
  FileUploadModule,
  DialogModule,
  ConfirmDialogModule,
  ToastModule,
  DropdownModule,
  CheckboxModule,
  ListboxModule,
  MultiSelectModule,
  ChipsModule,
  TooltipModule,
  PasswordModule,
  DividerModule,
  FontAwesomeModule,
  PanelModule
];

const EXPORTS = [
  PaginationModule,
  PagingHeaderComponent,
  PagerComponent,
  CarouselModule,
  OrderTotalsComponent,
  ReactiveFormsModule,
  FormsModule,
  BsDropdownModule,
  TextInputComponent,
  CdkStepperModule,
  StepperComponent,
  BasketSummaryComponent,
  NgxGalleryModule,
  TabsModule,
  NgxDropzoneModule,
  ImageCropperModule,
  TableModule,
  ToolbarModule,
  FileUploadModule,
  DialogModule,
  ConfirmDialogModule,
  ToastModule,
  DropdownModule,
  CheckboxModule,
  PhotoWidgetComponent,
  SelectDropdownComponent,
  MultiSelectModule,
  MeassureNamePipe,
  ShopMapComponent,
  CollapsePanelComponent,
  OrderCountComponent,
  ChipsModule,
  TooltipModule,
  PasswordModule,
  DividerModule,
  FontAwesomeModule,
  PanelModule
]
@NgModule({
  declarations: [DECLARATIONS, ShopMapComponent],
  imports: [
    IMPORTS
  ],
  exports: [
    EXPORTS
  ]
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIcons(faCoffee);
  }
}
