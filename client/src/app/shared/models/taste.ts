export interface ITaste {
  id?:number;
  name:string;
  description:string;
}
