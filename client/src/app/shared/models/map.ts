export interface IMapProps {
  lat: number;
  lng: number;
  title: string;
  id:number;
  zoom:number;

}
