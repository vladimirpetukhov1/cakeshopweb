import { Component, Input, OnInit } from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';
import { IMapProps } from '../../models/map';

@Component({
  selector: 'app-shop-map',
  templateUrl: './shop-map.component.html',
  styleUrls: ['./shop-map.component.scss']
})


export class ShopMapComponent implements OnInit {


  map: google.maps.Map;
  title: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;
  @Input() props: IMapProps;

  constructor() { }

  ngOnInit(): void {
    console.log(this.props)
    let loader = new Loader({
      apiKey: 'AIzaSyAIH7X8SP_pFTEh_9LgqZBJnsXyHHeLQc4'
    });

    const center={ lat: this.props.lat, lng: this.props.lng };
    loader.load().then(() => {
      this.map = new google.maps.Map(document.getElementById('map') as HTMLElement, {
        center: {lat:-33.712451, lng:150.311823},
        zoom: 20,
      });
    });
    const svgMarker = {
      path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
      fillColor: "blue",
      fillOpacity: 0.6,
      strokeWeight: 0,
      rotation: 0,
      scale: 2,
      anchor: new google.maps.Point(15, 30),
    };
    const marker = new google.maps.Marker({
      position: this.map.getCenter(),
      map: this.map,
      icon:svgMarker
    });
  }

}
