import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ShopParams } from '../../models/shopParams';

const
DEFAULT_DURATION = 500;

@Component({
  selector: 'app-collapse-panel',
  templateUrl: './collapse-panel.component.html',
  styleUrls: ['./collapse-panel.component.scss'],
  animations: [
    trigger('collapse', [
      state('true', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
      state('false', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(DEFAULT_DURATION + 'ms ease-in')),
      transition('true => false', animate(DEFAULT_DURATION + 'ms ease-out'))
    ])
  ]
})
export class CollapsePanelComponent implements OnInit {


  @Input() items: any;

  @Input() title: string;

  @Output() itemClicked = new EventEmitter<number>();

  @Input() shopParams: ShopParams;

  collapsed:boolean;
  constructor() {
    this.collapsed=false;
   }

  ngOnInit(): void {
  }

  onItemSelected(id: number) {
    this.itemClicked.emit(id);
  }

  toggle(){
    this.collapsed = !this.collapsed;
  }

}
