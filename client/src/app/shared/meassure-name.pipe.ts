
import { MeassureService } from './../admin/services/meassure.service';
import { IMeassure } from './models/product';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'meassureName'
})
export class MeassureNamePipe implements PipeTransform {

  transform(value: IMeassure[], meassureId:number): unknown {
    return value.find(i=> i.id==meassureId).name;
  }

}
