import { ForgottenPassowrdComponent } from './forgottenPassowrd/forgottenPassowrd.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, data: { breadcrumb: 'Вход' } },
  { path: 'register', component: RegisterComponent, data: { breadcrumb: 'Регистрация' } },
  { path: 'forgottenPassword', component: ForgottenPassowrdComponent, data: { breadcrumb: 'Забравена парола' } },
  { path: 'forgotpassword', component: ForgotPasswordComponent ,data: { breadcrumb: 'Забравена парола' }},
  { path: 'resetpassword', component: ResetPasswordComponent, data: { breadcrumb: 'Промяна на парола' } }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
