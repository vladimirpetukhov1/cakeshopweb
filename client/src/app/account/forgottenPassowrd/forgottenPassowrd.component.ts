import { AccountService } from 'src/app/account/account.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgottenPassowrd',
  templateUrl: './forgottenPassowrd.component.html',
  styleUrls: ['./forgottenPassowrd.component.scss']
})
export class ForgottenPassowrdComponent implements OnInit {

  passwordForm: FormGroup;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.createPasswordForm();
  }

  onSubmit() {
    this.accountService.forgottenPassword(this.passwordForm).subscribe((res) => {
      console.log(res)
    });
  }

  createPasswordForm() {
    this.passwordForm = new FormGroup({
      email: new FormControl('')
    })
  }




}
