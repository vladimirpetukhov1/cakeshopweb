import { TasteService } from './../../admin/services/taste.service';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { BrandTypeService } from 'src/app/admin/services/brand-type.service';
import { BrandsService } from 'src/app/admin/services/brands.service';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor(
    private brandService: BrandsService,
    private typeService: BrandTypeService,
    private tasteService:TasteService
  ) { }

  //TODO: make this methid generik
  uniqueBrandName(): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      return this.brandService.isNameUnique(c.value).pipe(
        tap((res) => { console.log(res) }),
        map(res => {
          if (res) {
            return { 'nameIsTaken': true };
          }
          return null;
        })
      );
    };
  }

  uniqueTypeName(): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      return this.typeService.isNameUnique(c.value).pipe(
        tap((res) => { console.log(res) }),
        map(res => {
          if (res) {
            return { 'nameIsTaken': true };
          }
          return null;
        })
      );
    };
  }

  uniqueTasteName(): AsyncValidatorFn {
    return (c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      return this.tasteService.isNameUnique(c.value).pipe(
        tap((res) => { console.log(res) }),
        map(res => {
          if (res) {
            return { 'nameIsTaken': true };
          }
          return null;
        })
      );
    };
  }
}
