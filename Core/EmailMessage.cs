﻿namespace Core
{
    using Microsoft.AspNetCore.Http;
    using MimeKit;

    public class EmailMessage
    {
        public MailboxAddress To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }

        public IFormFileCollection Attachments { get; set; }

        public EmailMessage(string to, string subject, string content, IFormFileCollection attachments)
        {
            To = new MailboxAddress(to,to);
            Subject = subject;
            Content = content;
            Attachments = attachments;
        }


    }
}
