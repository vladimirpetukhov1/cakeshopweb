using System.Security.AccessControl;
namespace Core.Entities
{
    
    public class ProductBrand : BaseEntity
    {
        public string Name { get; set; }
    }
}