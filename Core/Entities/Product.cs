using System.Collections.Generic;
using System.Linq;

namespace Core.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public string PictureUrl { get; set; }

        public ProductType ProductType { get; set; }
        public int? ProductTypeId { get; set; }

        public ProductBrand ProductBrand { get; set; }
        public int? ProductBrandId { get; set; }
        
        public readonly List<ProductPrice> _prices = new List<ProductPrice>();
        public IReadOnlyList<ProductPrice> Prices => _prices.AsReadOnly();

        private readonly List<Photo> _photos = new List<Photo>();
        public IReadOnlyList<Photo> Photos => _photos.AsReadOnly();

        public void AddPhoto(string pictureUrl, string fileName, bool isMain = false)
        {
            var photo = new Photo
            {
                FileName = fileName,
                PictureUrl = pictureUrl
            };

            if (_photos.Count == 0) photo.IsMain = true;

            _photos.Add(photo);
        }

        public void RemovePhoto(int id)
        {
            var photo = _photos.Find(x => x.Id == id);
            _photos.Remove(photo);
        }

        public void SetMainPhoto(int id)
        {
            var currentMain = _photos.SingleOrDefault(item => item.IsMain);
            foreach (var item in _photos.Where(item => item.IsMain))
            {
                item.IsMain = false;
            }

            var photo = _photos.Find(x => x.Id == id);
            if (photo != null)
            {
                photo.IsMain = true;
                if (currentMain != null) currentMain.IsMain = false;
            }
        }

        public void AddPrice(ProductPrice price)
        {
            if (_prices.Count == 0) price.IsMain = true;

            if (price != null)
                _prices.Add(price);
        }

        public void UpdatePrice(ProductPrice price)
        {
            var current = _prices.Find(_ => _.Id == price.Id);
            current.Value = price.Value;
            current.Quantity = price.Quantity;
            current.MeassureId = price.MeassureId;
        }

        public void RemovePrice(int id)
        {
            var price = _prices.Find(i => i.Id == id);

            _prices.Remove(price);
        }

        public void SetMainPrice(int id)
        {
            var currentMain = _prices.SingleOrDefault(_ => _.IsMain == true);

            foreach (var p in _prices.Where(p => p.IsMain))
            {
                p.IsMain = false;
            }

            var price = _prices.Find(i => i.Id == id);
            if (price != null)
            {
                price.IsMain = true;
                if (currentMain != null) currentMain.IsMain = false;
            }

        }

    }
}