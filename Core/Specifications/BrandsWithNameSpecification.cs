﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Specifications
{
    public class BrandsWithNameSpecification : BaseSpecifcation<ProductBrand>
    {
        public BrandsWithNameSpecification(BrandSpecParams brandParams) : base()
        {
            ApplyPaging(brandParams.PageSize * (brandParams.PageIndex - 1), brandParams.PageSize);

            if (!string.IsNullOrEmpty(brandParams.Sort))
            {
                AddOrderBy(n => n.Name);
            }
        }

    }
}

