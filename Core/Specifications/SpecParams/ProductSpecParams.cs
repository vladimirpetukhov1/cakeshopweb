using Core.Entities;
using System.Collections.Generic;

namespace Core.Specifications
{
    public class ProductSpecParams : BaseSpecParams
    {

        public int? BrandId { get; set; }
        public int? TypeId { get; set; }
        public int? TasteId { get; set; }
        public string Sort { get; set; }

        private string _search;
        public string Search
        {
            get => _search;
            set => _search = value.ToLower();
        }
    }
}