﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Specifications
{
    public class TastesWithNamesSpecification: BaseSpecifcation<Taste>
    {
        public TastesWithNamesSpecification(TasteSpecParams tasteParams) : base()
        {
            ApplyPaging(tasteParams.PageSize * (tasteParams.PageIndex - 1), tasteParams.PageSize);

            if (!string.IsNullOrEmpty(tasteParams.Sort))
            {
                AddOrderBy(n => n.Name);
            }
        }
    }
}
