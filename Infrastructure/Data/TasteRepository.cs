using Core.Interfaces;

namespace Infrastructure.Data
{
    public class TasteRepository:ITasteRepository
    {
        private readonly StoreContext _context;
        
        public TasteRepository(StoreContext context)
        => _context = context;
        
    }
}