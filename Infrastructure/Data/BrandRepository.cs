﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class BrandRepository : IBrandRepository
    {
        private readonly StoreContext _context;

        public BrandRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task<ProductBrand> GetBrandByIdAsync(int id)
        {
            return await _context.ProductBrands.FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IReadOnlyList<ProductBrand>> GetBrandsAsync()
        {
            return await _context.ProductBrands.ToListAsync();
        }
    }
}
