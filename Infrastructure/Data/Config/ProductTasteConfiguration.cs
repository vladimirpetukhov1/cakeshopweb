﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.Config
{
    class ProductTasteConfiguration : IEntityTypeConfiguration<ProductTaste>
    {
        public void Configure(EntityTypeBuilder<ProductTaste> builder)
        {
            builder.HasKey(k => new
            {
                k.ProductId,
                k.TasteId
            });
        }
    }
}
