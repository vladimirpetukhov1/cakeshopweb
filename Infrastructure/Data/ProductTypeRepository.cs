﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class ProductTypeRepository:IProductTypeRepository
    {
        private readonly StoreContext _context;

        public ProductTypeRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task<ProductType> GetTypeByIdAsync(int id)
        {
            return await _context.ProductTypes.FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IReadOnlyList<ProductType>> GetBrandsAsync()
        {
            return await _context.ProductTypes.ToListAsync();
        }
    }
}
